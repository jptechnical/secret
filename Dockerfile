FROM golang:1.15 AS builder

ENV GO111MODULE=on

WORKDIR /go/src/gitlab.com/jptechnical/secret

ADD . .

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -o secret .


FROM alpine:latest

ENV \
    VAULT_ADDR \
    VAULT_TOKEN \
    SUPERSECRETMESSAGE_HTTP_BINDING_ADDRESS \
    SUPERSECRETMESSAGE_HTTPS_BINDING_ADDRESS \
    SUPERSECRETMESSAGE_HTTPS_REDIRECT_ENABLED \
    SUPERSECRETMESSAGE_TLS_AUTO_DOMAIN \
    SUPERSECRETMESSAGE_TLS_CERT_FILEPATH \
    SUPERSECRETMESSAGE_TLS_CERT_KEY_FILEPATH

RUN \
    apk add --no-cache ca-certificates ;\
    mkdir -p /opt/secret/static

WORKDIR /opt/secret

COPY --from=builder /go/src/gitlab.com/jptechnical/secret/secret .
COPY static /opt/secret/static

CMD [ "./secret" ]
